<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

define('DB_USER', $_ENV['OPENSHIFT_MYSQL_DB_USERNAME']);

/** MySQL database password */
define('DB_PASSWORD', $_ENV['OPENSHIFT_MYSQL_DB_PASSWORD']);

/** MySQL hostname */
define('DB_HOST', $_ENV['OPENSHIFT_MYSQL_DB_HOST'] . ':' . $_ENV['OPENSHIFT_MYSQL_DB_PORT']);

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('WP_ALLOW_REPAIR', true);




/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '&-{hrt4*utWn6 1WSw.+%(7D,jh{+#A$[ z?p|I.:~^>srV#M:FE2:zv.L]l)Sy$');
define('SECURE_AUTH_KEY',  'Z/n42FKLlm(w%>C}K1}C<8W?GKn<D7H.4z3|J-t$5ovBD;VR_|z-De?I7+M_c-@w');
define('LOGGED_IN_KEY',    '@&I9&)l#08qdPo??hB+kYP w<V,86!{`[DKI[-V{Gem0Po>;?ltdmvpJqAOw@Y7w');
define('NONCE_KEY',        ',_eF srz-P{@gC:Qyb~XCmQ.&IT{f[gWo1: `:;k-3qr.ks}D!GLNfDWnQ0SI^77');
define('AUTH_SALT',        '7aW1ll!}mQr*t=c{xl9le|5|:^~jm*_r:l#`05!{Nbjl*!tCfwvVS$B?-}H*L8_Y');
define('SECURE_AUTH_SALT', 'ZCqUb,J?2QrGG=xD(TarND7f?fIP,sLL!JOi7-4prb1s&rFy]Cooc&xB>J.Xn5Go');
define('LOGGED_IN_SALT',   '!<tY]E%GHg!JtjZY7x_8)lr;T/lmYYM.HH8O~^O$fOce{&Mn.ow5+Gx(sPY}^ru&');
define('NONCE_SALT',       'nD/p_>w8;+cI&-A@8cj1;3H7%i@SAve[)RvB<+hQ<$-%V?CyKR1xG6rj{m?DX{:V');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

