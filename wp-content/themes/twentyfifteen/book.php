<?php
/**
 *
 * Template Name: Book
 */

get_header();

 ?>
 
    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">
        <?php
            //get current url
            $slug = pods_v( 'last', 'url' );
            //get pod name
            $pod_name = pods_v( 0, 'url');
            //get pods object for current item
            $pods = pods( $pod_name, $slug );
            
            $pods->fetch();
            $title = $pods->display('titulo');
        ?>

            <article>
                <?php echo $titulo;?>
 
                <!--<?php
                    //Output template of the same name as Pod, if such a template exists.
                    $temp = $pods->template($pod_name);
                    if ( isset($temp)  ) {
                        echo $temp;
                    }
                ?>-->
            </article><!-- #post -->
 
        </div><!-- #content -->
    </div><!-- #primary -->
 
<?php get_sidebar(); ?>
<?php get_footer(); ?>