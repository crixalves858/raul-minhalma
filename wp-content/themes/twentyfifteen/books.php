<?php
/**
 *  Template Name: Books
 */

get_header(); ?>
 
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<?php
                //set find parameters
                $params = array( 'limit' => 4 );
                //get pods object
				$pods = pods( 'livro', $params );
                //loop through records
                if ( $pods->total() > 0 ) {
                    while ( $pods->fetch() ) {
                        //Put field values into variables
                        $title = $pods->display('titulo');
                        $permalink = site_url( 'spaceship/' . $pods->field('permalink' ) );
                        $picture = $pods->field('foto');
                        $id = $pods->field('id');
            ?>
                        <a href="<?php echo esc_url( $permalink); ?>">
                            <article>
                                <header class="entry-header">
                                    <?php if ( ! is_null($picture) ) : ?>
                                        <div class="grid">
                                            <div class="col-third">
                                                <div class="entry-thumbnail">
                                                    <?php echo wp_get_attachment_image( $picture['ID'] ); ?>
                                                </div>
                                            </div>
                                            <div class="col-2third">
                                                <h1 class="entry-title">
                                                    <a href="<?php echo esc_url( $permalink); ?>" rel="bookmark"><?php _e( $title , 'PP2014' ); ?></a>
                                                </h1>
                                            </div>
                                        </div>
                                    <?php else : ?>
                                        <h1 class="entry-title">
                                            <a href="<?php echo $permalink; ?>" rel="bookmark"><?php echo $title; ?></a>
                                        </h1>
                                    <?php endif; ?>
                                </header><!-- .entry-header -->

                            </article><!-- #post -->
                        </a>
            <?php
                    } //endwhile
                } //endif

                //do the pagination
                echo $pods->pagination( array( 'type' => 'advanced' ) );
            ?>
		</div><!-- #content -->
	</div><!-- #primary -->
 
 
<?php get_sidebar(); ?>
<?php get_footer(); ?>