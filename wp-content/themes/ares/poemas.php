<?php
/**
 *  Template Name: Poemas
 */

get_header();
?>

<div class="site-content-wrapper <?php echo esc_attr( of_get_option('ares_theme_background_pattern','crossword') ); ?>">
    <div id="content" class="site-content site-content-wrapper crossword">
        <div class="page-content row">
           <article class="col-md-1 item-page col1" >
                <h2 class="post-title">Poemas</h2>
                

                  

                            <?php
                                //set find parameters
                                special_nav_class([],"");

                                $params = array(
                                        'orderby' => 'date DESC',
                                        'limit' => 9, // Returns all
                                );

                                $pods = pods( 'poemas', $params );
                                //loop through records
                                $i = 0;

                                if ( $pods->total() > 0 ) {
                                    
                                    while ( $pods->fetch() ) {
                                    //Put field values into variables
                                        
                                        $title = $pods->display('post_title');
                                        $permalink = $pods->field('permalink' ) ;
                                        
                                        $row = $pods->row();
                                        $post_id = $row['ID']; 
                                        $image_id = get_post_thumbnail_id($post_id);
                                        $image = wp_get_attachment_image_src($image_id,'full');
                                        $image_url = $image[0];                                 
                                        $content = $pods->field('post_content'); 
                                        $date = $pods->display('post_date');
                                      
                                        if(($i%3) == 0) { ?>
                                        <div class="post-row page-content col-md-12 ">

                                        <?php
                                        }
                                        $i++;
                                        ?>
                                        
                                        <div class="col-md-4 center"  >
                                            <?php if(!empty($image_url)) { ?>

                                            
                                            <div class="post-thumb row" style="  width: 100%;   height: 200px;">
                                                <a href="<?php echo $permalink ; ?>">
                                                    <img  src="<?php  echo $image_url;  ?>"
                                                </a>
                                            </div>
                                            <?php } ?>
                                            <div style="width: 100%; text-align: justify; text-justify: inter-word;">
                                                <h5 style="font-size: smaller;">  <?php echo date_i18n( get_option( 'date_format' ), strtotime( $date ) ); ?> </h5>
                                                <h2 class="post-title">
                                                    <a href="<?php echo esc_url( $permalink); ?>">
                                                        <?php echo $title; ?>
                                                    </a>
                                                </h2>
                                                <div class="post-content">
                                                    <?php echo wp_trim_words( $content, 50); ?>
                                                </div>
                                                <div class="text-right">
                                                    <a href="<?php echo $permalink ; ?>">Continuar a ler</a>
                                                </div>                        
                                            </div>
                                        </div>

                                        <?php
                                       
                                        if(($i%3) == 0 || $i == $pods->total()) { ?>

                                        </div>

                                        <?php
                                        } 
                                      
                                    } //endwhile
                                } //endif
                            ?>
                             <?php echo $pods->pagination( array( 'type' => 'paginate' ) ); ?>
            </article>

        </div>
    </div>
</div>
<?php get_footer(); ?>