<?php
/**
 *
 * Template Name: Video
 */

get_header();
?>

<div class="site-content-wrapper <?php echo esc_attr( of_get_option('ares_theme_background_pattern','crossword') ); ?>">
    <div id="content" class="site-content site-content-wrapper crossword">
        <div class="page-content row">
        <?php
            //get current url
            $slug = pods_v( 'last', 'url' );

             $pod_name = "videos";
            //get pods object for current item
            $pods = pods('videos', $slug );

        ?>

            <article >
                <?php
                    //Output template of the same name as Pod, if such a template exists.
                    $temp = $pods->template('video');
                    if ( isset($temp)  ) {
                        echo $temp;
                    }
                ?>
            </article><!-- #post -->
 
        </div><!-- #content -->
    </div><!-- #primary -->
 </div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>