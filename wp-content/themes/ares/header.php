<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package ares
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

        <link rel="icon" href="<?php bloginfo('template_url'); ?>/images/logo.ico" type="image/x-icon" />
        <link rel="shortcut icon" href="<?php bloginfo('template_url'); ?>/images/logo.ico" type="image/x-icon" />

        <?php wp_head(); ?>
           <!-- js -->



<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery-1.8.3.min.js"></script>

<!--<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.browser.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.browser.min.js"></script>-->

<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/modernizr.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/imagesloaded.pkgd.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/imagesloaded.pkgd.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/js/fancybox/jquery.fancybox-1.3.4.css" media="screen" />
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/inc/css/font/stylesheet.css" type="text/css" charset="utf-8" />
<script>
    $(document).ready(function(){
        if (Modernizr.touch) {
            // show the close overlay button
            $(".close-overlay").removeClass("hidden");
            // handle the adding of hover class when clicked
            $(".img").click(function(e){
                if (!$(this).hasClass("hover")) {
                    $(this).addClass("hover");
                }
            });
            // handle the closing of the overlay
            $(".close-overlay").click(function(e){
                e.preventDefault();
                e.stopPropagation();
                if ($(this).closest(".img").hasClass("hover")) {
                    $(this).closest(".img").removeClass("hover");
                }
            });
        } else {
            // handle the mouseenter functionality
            $(".img").mouseenter(function(){
                $(this).addClass("hover");
            })
            // handle the mouseleave functionality
            .mouseleave(function(){
                $(this).removeClass("hover");
            });
        }
    });
</script>
    </head>

    <body <?php body_class(); ?>>
        <div id="page" class="hfeed site <?php echo 'no' == of_get_option('ares_headerbar_bool', 'yes') ? 'no_toolbar' : ''; ?>">
            <header id="masthead" class="site-header" role="banner">
                <?php ares_toolbar(); ?>
                <div class="site-branding">
                    <div class="row ">
                            <div class="col-xs-3">
                                <div id="tasty-mobile-toggle">
                                    
                                    <i class="fa fa-bars"></i>
                                    <?php wp_nav_menu(array('theme_location' => 'primary')); ?>
                                </div>
                                
                                <h2 class="site-title">
                                    <a href="<?php echo esc_url(home_url('/')); ?>" rel="home">
                                        <?php if (of_get_option('ares_logo_image') != '') : ?>
                                        <img src="<?php echo esc_attr( of_get_option('ares_logo_image') ); ?>" alt="" id="sc_logo"/>
                                        <?php else : ?>
                                        <?php bloginfo('name');?>
                                        <?php endif; ?>                                        
                                    </a>
                                </h2>
                                <?php if (of_get_option('ares_logo_image') == '') : ?>
                                    <h3 class="site-description"><?php bloginfo('description'); ?></h3>
                                <?php endif; ?>

                            </div>
                            <div class="col-xs-9 menu-bar">
                                <nav id="site-navigation" class="main-navigation" role="navigation">
                                    <?php wp_nav_menu(array('theme_location' => 'primary')); ?>
                                </nav>
                            </div>
                    </div>
                </div>

            </header><!-- #masthead -->


