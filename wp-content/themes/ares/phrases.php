<?php
/**
 *  Template Name: Phrases
 */

get_header();
?>

<div class="site-content-wrapper <?php echo esc_attr( of_get_option('ares_theme_background_pattern','crossword') ); ?>">
    <div id="content" class="site-content site-content-wrapper crossword">
        <div class="page-content row">
           <article class="col-md-1 item-page col1" >
                <h2 class="post-title">Frases</h2>
                    <div class="container">

                  

                            <?php
                                //set find parameters
                               

                                $params = array(
                                        'orderby' => 'date DESC',
                                        'limit' => -1, // Returns all
                                );

                                $pods = pods( 'frases', $params );
                                //loop through records
                                $i = 0;

                                if ( $pods->total() > 0 ) {
                                    
                                    while ( $pods->fetch() ) {
                                    //Put field values into variables
                                        
                                        $title = $pods->display('post_title');
                                        $permalink = $pods->field('permalink' ) ;
                                        
                                        $row = $pods->row();
                                        $post_id = $row['ID']; 
                                        $image_id = get_post_thumbnail_id($post_id);
                                        $image = wp_get_attachment_image_src($image_id,'full');
                                        $image_url = $image[0];                                 
                                        $content = $pods->field('post_content'); 
                                        $date = $pods->display('post_date');
                                        ?>

                                        <div class="item" >
                                          
                                            <a href="<?php echo $permalink ; ?>">
                                                <img src="<?php  echo $image_url;  ?>" />
                                            </a>
                                          
                                            
                                        </div>

                                     

                                        <?php
                                         
                                      
                                    } //endwhile
                                } //endif
                            ?>
                        </div>
                            
            </article>

        </div>
    </div>
</div>


<script>



imagesLoaded( document.querySelector('.container'), function( instance ) {
var container = document.querySelector('.container');
      var msnry = new Masonry( container, {
        columnWidth: 30
      });
});


/*
docReady( function() {
  $('.container').imagesLoaded( function() {
    var container = document.querySelector('.container');
      var msnry = new Masonry( container, {
        columnWidth: 30
      });
    });

});*/
</script>
  
  </script>
<?php get_footer(); ?>