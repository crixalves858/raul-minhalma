<?php
/**
 *  Template Name: Books
 */

get_header();
?>

<div class="site-content-wrapper <?php echo esc_attr( of_get_option('ares_theme_background_pattern','crossword') ); ?>">
    <div id="content" class="site-content site-content-wrapper crossword">
        <div class="page-content row">
           <article class="col-md-9 item-page" style="width:100%">
                <h2 class="post-title">Livros</h2>
                <div class="row">

                    <div class=" page-content col-md-12 center">
                        <div >
                            <?php

                                special_nav_class([],"menu-item-39");
                                //set find parameters
                                $params = array( 'limit' => 4 );
                                //get pods object
                                $pods = pods( 'livro', $params );
                                //loop through records
                                if ( $pods->total() > 0 ) {
                                    while ( $pods->fetch() ) {
                                    //Put field values into variables
                                        $title = $pods->display('titulo');
                                        $permalink = $pods->field('permalink' ) ;
                                        $picture = $pods->field('foto');
                                        $id = $pods->field('id'); ?>
                                       
                                           
                                            <div class="col-md-4 center">
                                           <div id="effect-5" class="effects clearfix">
                                                <div class="img">
                                                    <?php echo wp_get_attachment_image( $picture['ID'], medium  ); ?>
                                                    <div class="overlay">
                                                        <a href="<?php echo esc_url( $permalink); ?>" class="expand">+</a>
                                                        <a class="close-overlay hidden">x</a>
                                                    </div>
                                                </div>
                                               
                                                <a href="<?php echo esc_url( $permalink); ?>">
                                                    <h1 class="entry-title" >
                                                        <?php echo $title; ?>
                                                    </h1>
                                                </a>
                                                
                                            </div>
                                        </div>


                                           
                                        
                                        <?php
                                    } //endwhile
                                } //endif
                            ?>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </div>
</div>
<?php get_footer(); ?>