<?php
/**
 *  Template Name: Videos
 */

get_header();
?>

<div class="site-content-wrapper <?php echo esc_attr( of_get_option('ares_theme_background_pattern','crossword') ); ?>">
    <div id="content" class="site-content site-content-wrapper crossword">
        <div class="page-content row">
           <article class="col-md-1 item-page col1" >
                <h2 class="post-title">Textos</h2>
                    <div class="col-md-12">

                            <?php
                                //set find parameters
                                special_nav_class([],"");
                               $params = array(
                                        'orderby' => 'date DESC',
                                        'limit' => 9, // Returns all
                                );

                                $pods = pods( 'videos', $params );
                                //loop through records
                                $i = 0;

                                if ( $pods->total() > 0 ) {
                                    
                                    while ( $pods->fetch() ) {
                                    //Put field values into variables
                                        
                                        $title = $pods->display('post_title');
                                        $permalink = $pods->field('permalink' ) ;
                                        
                                        $row = $pods->row();
                                        $post_id = $row['ID']; 
                                       
                                        $date = $pods->display('post_date');

                                        $youtube = $pods->field('link_youtube');
                                      
                                       ?>
                                        
                                        <div class="col-md-6 ">
                                            <div style="margin-left: 50px;">
                                                <h5 style="font-size: smaller;">  <?php echo date_i18n( get_option( 'date_format' ), strtotime( $date ) ); ?> </h5>
                                                <h2 class="post-title">
                                                    <a >
                                                        <?php echo $title; ?>
                                                    </a>
                                                </h2>
                                            </div>
                                            <a href="#">
                                            <div class="center">
                                                <p><iframe width="420" height="315" src="<?php echo $youtube; ?>" frameborder="0" allowfullscreen></iframe></p>                   
                                            </div>
                                            </a>
                                        </div>

                                       <?php
                                      
                                    } //endwhile
                                } //endif
                            ?>
                             <?php echo $pods->pagination( array( 'type' => 'paginate' ) ); ?>
                </div>
            </article>

        </div>
    </div>
</div>
<?php get_footer(); ?>