<?php
/**
 *
 * Template Name: Phrase
 */

get_header();
?>

<div class="site-content-wrapper <?php echo esc_attr( of_get_option('ares_theme_background_pattern','crossword') ); ?>">
    <div id="content" class="site-content site-content-wrapper crossword">
        <div class="page-content row">
        <?php
            //get current url
            $slug = pods_v( 'last', 'url' );

            
            $pod_name = "frases";
            //get pods object for current item
            $pods = pods('frases', $slug );

        ?>

            <article >
                <?php
                    //Output template of the same name as Pod, if such a template exists.
                    $temp = $pods->template('frase');
                   
                    if ( isset($temp)  ) {
                        echo $temp;
                    }
                ?>
                <br/>
                 <div class="fb-like" data-href="<?php echo "http://".$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
                <div id="comments" class="comments-area">
                    <div id="respond" class="comment-respond">
                        <div class="fb-social-plugin comment-form fb-comments fb_iframe_widget" id="commentform" data-href="<?php echo "http://".$_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] ?>" data-width="1060" data-order-by="social" fb-xfbml-state="rendered">
                    </div>
                </div>
            </article><!-- #post -->
        </div><!-- #content -->

    </div><!-- #primary -->
 </div>
<?php get_footer(); ?>