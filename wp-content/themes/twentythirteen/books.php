<?php
/**
 *  Template Name: Books
 */

get_header(); ?>
 
    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">
            <div id="row">
            <?php
                //set find parameters
                $params = array( 'limit' => 4 );
                //get pods object
                $pods = pods( 'livro', $params );
                //loop through records
                if ( $pods->total() > 0 ) {
                    while ( $pods->fetch() ) {
                        //Put field values into variables
                        $title = $pods->display('titulo');
                        $permalink = $pods->field('permalink' ) ;
                        $picture = $pods->field('foto');
                        $id = $pods->field('id');
            ?>
                        <div class="col-md-4">
                        <a href="<?php echo esc_url( $permalink); ?>">
                            <article>
                                <header class="entry-header">
                                    <div class="grid">
                                        <div class="col-third">
                                            <div class="entry-thumbnail">
                                                <?php echo wp_get_attachment_image( $picture['ID'] ); ?>
                                            </div>
                                        </div>
                                        <div class="col-2third">
                                            <h1 class="entry-title">
                                               <?php echo $title; ?>
                                            </h1>
                                        </div>
                                    </div>
                                </header><!-- .entry-header -->

                            </article><!-- #post -->
                        </a>
                    </div>
            <?php
                    } //endwhile
                } //endif

                //do the pagination
                echo $pods->pagination( array( 'type' => 'advanced' ) );
            ?>
        </div><!-- #content -->
    </div><!-- #primary -->
 
 
<?php get_sidebar(); ?>
<?php get_footer(); ?>