<?php
/**
 *
 * Template Name: Book
 */

get_header();

 ?>

    <div id="primary" class="content-area">
        <div id="content" class="site-content" role="main">
        <?php
            //get current url
            $slug = pods_v( 'last', 'url' );

             $pod_name = "livro";
            //get pods object for current item
            $pods = pods('livro', $slug );
            
          
            
            
        ?>

            <article>
       
 
                <?php
                    //Output template of the same name as Pod, if such a template exists.
                    $temp = $pods->template('livro');
                    if ( isset($temp)  ) {
                        echo $temp;
                    }
                ?>
            </article><!-- #post -->
 
        </div><!-- #content -->
    </div><!-- #primary -->
 
<?php get_sidebar(); ?>
<?php get_footer(); ?>